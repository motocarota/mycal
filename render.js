const _ = require('lodash')
const { load } = require('./utils')
const {
  DAYS,
  LABELS,
  OK_SYMBOL,
  KO_SYMBOL,
  CELL_SIZE,
 } = require('./constants.js')

function renderCell(v) {
  const value = (!!v)
    ? (v === 1)
      ? OK_SYMBOL
      : String(v).slice(0, CELL_SIZE) // if we specify a special char
    : KO_SYMBOL

  return _.padStart(value, CELL_SIZE, '   ')
}

function renderRow(values, index) {
  const label = _.get(LABELS, index)
  const rowData = _.map(
    values,
    renderCell
  )
  const res = `${label} ${rowData.join('')}`

  console.log(res)
}

function renderHeader(objective) {
  const header = `    ${DAYS.map(d => _.padStart(d, CELL_SIZE, ' ')).join('')}`
  const d = new Date().toLocaleDateString()
  const o = objective 
    ? `Objective: ${objective}`
    : ''

  console.log('----------------------------------------------------------------')
  console.log(`  ${d}    ${o}`)
  console.log('----------------------------------------------------------------')
  console.log(header)
}

function render(objective) {
  renderHeader(objective)
  const data = load(objective)
  _.forEach(data, renderRow)
}

module.exports = {
  render
}