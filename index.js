#!/usr/bin/env node

const yargs = require('yargs')
const { render } = require('./render')
const { update } = require('./utils')

const argv = yargs
  .command('ok', 'turns on today, to keep track of your daily objective', {
    value: {
      description: 'the value to set',
      alias: 'v',
      type: 'string',
    }
  })
  .command('ko', 'turns off today if you activated it by mistake')
  .option('date', {
    alias: 'd',
    description: 'the specific date to update',
    type: 'string',
  })
  .option('objective', {
    alias: 'o',
    description: 'the objective you want to achieve and track',
    type: 'string',
  })
  .help()
  .alias('help', 'h')
  .argv

let objective = ''
if (argv.objective) {
  objective = argv.objective
}

if (argv._.includes('ok')) {
  const value = argv.value || 1

  update({
    value,
    ts: argv.date,
    objective,
  })
}

if (argv._.includes('ko')) {
  update({
    value: 0,
    ts: argv.date,
    objective
  })
}

// console.log(argv)
render(objective)