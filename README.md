# myCal

A silly util to keep track of your daily progession towards a goal during the current year.

Your records will be saved into a json file in `~/.mycal_<CURRENT YEAR>.json` path

## WHY

[As a guy once suggested](https://www.youtube.com/watch?v=Pm9CQn07OjU), you can improve your chances of succeeding your long-term goals if you follow these rules:

- have written, clear and specific goals
- address at least 2 minutes every day to practice
- keep a track record of your progress
- rely on your environment to support your willpower

the third element is where this program comes in handy: you can track your daily 2 minutes practice task

## HOW

This program just tracks your progress on a json file and shows you your progress as the shiny board that guy had in his video.
The rest is up to you.

## Commands:

### show report 

`> mycal sport`

just renders the whole calendar

```
      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31
JAN   *  *  *  *     *  *  *        *  *  *
FEB         *  *     *     *  *  *  *  *  *     *  *  *     *  *        *  *
MAR               *
APR   *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
MAY   *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
JUN
JUL
AUG   *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *     *     *  *  *  *  *  *
SEP   *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
OCT                                    *
NOV      *  *        *  *        *  *  *
DIC                                                                                             *
```

### Mark today as done/undone

`> mycal ok`

turns "sport" activity for today as done

`> mycal ko`

turns off activity for today if you activated it by mistake

### Mark a specific day as done/undone

`> mycal ok --date 2020-12-31`

`> mycal ko --date 2020-12-31`

turns on/off an activity at a specific date

### Specify a custom objective

```
> mycal --objective marathon
> mycal ok --objective marathon
> mycal ko --objective marathon --date 2020-12-31
```

this will create a dedicated file for this activity, to be tracked. All the commands above can be called with a defined objective to give the command a context.

Your records for this specific objective will be saved into a `~/.mycal_<CURRENT YEAR>_<OBJECTIVE>.json` path.

This might be helpful if you need to track more than a single task (ex. gym, diet, drum lessons, ...)
