const fs = require('fs')
const _ = require('lodash')
const home = require('os').homedir()
const {
  EMPTY_SET,
  DEFAULT_OBJECTIVE,
} = require('./constants')

/*
  return the path of the file you should have your data into
*/
function getDataPath (objective) {
  const year = new Date().getFullYear()
  const filename = _.filter(['mycal', year, objective]).join('_')
  const path = `${home}/.${filename}.json`

  return path
}

/*
  returns the month column index from the input date
*/
function dateToCoords(ts) {
  const d = new Date(ts)

  return [
    d.getMonth(),
    d.getDate() - 1,
  ]
}

/*
  simply loads a data set from user's file
*/
function load (objective) {
  let content = EMPTY_SET
  const path = getDataPath(objective)

  try {
    const json = fs.readFileSync(path, 'utf8')
    const data = JSON.parse(json)
    content = data
  } catch {
    // no file found, creating new
    save(content, objective)
  }
   
  return content
}

/*
  simply stores a data set into user's file
*/
function save (data, objective) {
  const json = JSON.stringify(data, 0, 2)
  fs.writeFileSync(getDataPath(objective), json)
}

/* 
  updates the current data set

  examples 
  - to turn today ON
  update()
  update({ value: 1 })

  - to turn today OFF
  update({ value: 0 })

  - to turn a specific day ON
  update({ ts: '2020/03/05' })
  update({ ts: '2020/12/31', value: 1 })

  - to turn a specific day OFF
  update({ ts: '2020/12/31', value: 0 })
*/
function update (args = {}) {
  const {
    value = 1,
    ts = Date.now(),
    objective,
  } = args
  const coords = dateToCoords(ts)
  const data = load(objective)

  _.set(data, coords, value)
  save(data, objective)
}

module.exports = {
  save,
  load,
  update,
}
